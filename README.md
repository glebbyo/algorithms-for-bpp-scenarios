# README

This repo contains supplementary data for the paper titled **"Algorithms for the bin packing problem with scenarios"**, 
by: *Yulle G. F. Borges, Vinı́cius L. de Lima, Flávio K. Miyazawa, Lehilton L. C. Pedrosa, Thiago A. de Queiroz, Rafael C. S. Schouery*

More information can be found on the preprint available on [arXiv](https://arxiv.org/abs/2305.15351).

In this repo, you will find:
1. Instances 

---
## Instances
All instances used in the numerical experiments section of the paper can be found in directory `instances`. There are 12 classes of instances, with 10 instances of each class. . Classes are defined according to the following parameters: 
- Bin size (B): 100
- Item sizes (w_j): (0B, 1B]
- Number of items (n): 10, 50, 100, 200
- Number of scenarios (k): n, n/2, 2n
- Distribution of Scenarios: Each item belongs to each scenario with probability 1/2 independently (Bodis and Balogh)

Each class has a specific subdirectory, named after the values as `n_k`, containing 10 text file corresponding to an instance each.
Instance files are structured as follows:
```
number_of_items capacity_of_the_bin number_of_scenarios
for each item in items:
	weight_of_item number_of_scenarios_of_item
	k: for each k in scenarios_of_item
```

---

